# ECE 421 Assignment 1
# Group Members:
# - Chris Hut - hut@ualberta.ca
# - Vincent Phung - vzphung@ualberta.ca
# Group 5

# Usage:
# ruby assignment1_main.rb

# Run tests
# $ rake
# Tests work on lab machines, if they aren't working on another
# machine run $ bundle install to install required gems
require './sparselib.rb'

require './test/test_sparse.rb'
require './test/test_math.rb'

puts "For more examples, see tests/"
# Create an empty matrix
mat = SparseMatrix.new(3)
# create an identity matrix
mat2 = SparseMatrix.new([
  [1, 1, 1],
  [1, 1, 1],
  [1, 1, 1]])
# add matrices together
result = mat + mat2
puts mat.to_s + " + " + mat2.to_s + " = " + result.to_s

# create a tridiagonal matrix
mat3 = SparseMatrix.new([1, 2], [1, 2, 3], [1, 2])
puts "mat3.tridiagonal?: " + mat3.tridiagonal?.to_s
puts "mat.tridiagonal?: " + mat.tridiagonal?.to_s
puts "mat3: " + mat3.to_s
puts "mat: " + mat.to_s

# get determinant of matrix
det = mat3.determinant
puts "mat3 determinant: " + det.to_s

# get inverse of matrix
inv = mat3.inv
puts "inverse of mat3: " + inv.to_s

# multiply inverse and original to get identity
identity = inv * mat3
puts "inv * mat3: " + identity.to_s

# divide mat2 by mat3
quotient = mat2 / mat3
puts "mat2/mat3: " + quotient.to_s

# sparsity of matrices
puts "sparsity of mat2: " + mat2.sparsity.to_s
puts "sparsity of mat3: " + mat3.sparsity.to_s
puts "sparsity of mat: " + mat.sparsity.to_s
