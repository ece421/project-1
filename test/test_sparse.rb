require 'test/unit'
require './sparsematrix'

class TestSparseMatrix < Test::Unit::TestCase
  def setup
    @test = SparseMatrix.new(4,4)
  end

  def test_make_empty
    for r in 0..@test.rows-1
      for c in 0..@test.cols-1
        assert_equal 0, @test[r,c], "Empty array isn't all 0s"
      end
    end
  end

  def test_make_size
    matrix = SparseMatrix.new(65,54)
    assert_equal 65, matrix.rows, "Matrix didn't have 65 rows"
    assert_equal 54, matrix.cols, "Matrix didn't have 54 cols"

    matrix = SparseMatrix.new(100)
    assert_equal 100, matrix.rows, "Matrix didn't have 100 rows"
    assert_equal 100, matrix.cols, "Matrix didn't have 100 cols"
  end

  def test_square
    assert @test.square?
    
    matrix = SparseMatrix.new(100, 101)
    assert !matrix.square?, "Square matrix wasn't square"
  end

  def test_tridiagonal
    assert !@test.tridiagonal?
    array = [[4, 8, 0], [15, 16, 23], [0, 42, 108]]
    matrix = SparseMatrix.new(array)
    assert matrix.tridiagonal?, "Tridiagonal matrix isn't tridiagonal"
  end

  def test_make_array
    array = [[0, 1, 2], [3, 4, 5], [6, 7, 8]]
    matrix = SparseMatrix.new(array)
    assert matrix != nil, "Matrix was nil"
    for r in 0..2
      for c in 0..2
        assert_equal matrix[r,c], array[r][c], "Matrix didn't contain correct elements from array"
      end
    end
  end

  def test_equality
    array = [[0, 1, 2], [4, 8, 15], [16, 23, 42]]
    matrix = SparseMatrix.new(array)
    other_matrix = SparseMatrix.new(3,3)
    zero_array = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
    zero_mat = SparseMatrix.new(zero_array)

    assert_equal zero_mat, @test, "Zero matrices weren't eqaul"
    assert !(@test == matrix), "Empty matrix equaled a non empty one"
    assert_equal @test, @test, "The same empty matrix didn't equal itself"
    assert_equal matrix, matrix, "The same matrix, (with values), didn't equal itself"
    assert !(array == other_matrix), "Two different matrices equaled each other"
  end

  def test_equality_tridiagonal
    d1 = [4, 8]
    d2 = [15, 16, 23]
    d3 = [42, 108]
    tri_array = [[15, 4, 0], [42, 16, 8], [0, 108, 23]]
    diagonal_mat = SparseMatrix.new(d1, d2, d3)
    array_mat = SparseMatrix.new(tri_array)
    assert_equal diagonal_mat, array_mat, "Tridiagonal matrices created in different methods aren't equal"
  end

  def test_init_matrix_from_matrix
    mat = Matrix.zero(4)
    matrix = SparseMatrix.new(mat)
    assert matrix != nil, "Matrix was nil"
    assert matrix == @test, "Zero matrix made from Ruby matrix doesn't equal our sparse interpretation"
  end

  def test_make_tridiagonal_matrix
    d1 = [4, 8, 15]
    d2 = [16, 23, 42, 108]
    d3 = [108, 10, 99]
    matrix = SparseMatrix.new(d1, d2, d3)
    assert matrix != nil, "Tridiagonal matrix was nil"
    assert matrix.tridiagonal?
  end

  def test_get_column_zeros
    col = @test.column(0)
    assert col != nil, "Column was nil"

    assert_equal col, [0, 0, 0, 0], "Column wasn't all 0s"
  end

  def test_get_all_columns
    array = [[0,1],[9,0]]
    mat = SparseMatrix.new(array)
    col1 = mat.column(0)
    col2 = mat.column(1)

    assert_equal col1, [0,9], "First column wasn't equal"
    assert_equal col2, [1,0], "Second column wasn't equal"
  end

  def test_get_row_zeros
    row = @test.row(0)
    assert row != nil, "Row was nil"

    assert_equal row, [0, 0, 0, 0]
  end

  def test_get_all_rows
    array = [[4,0], [8,0]]
    mat = SparseMatrix.new(array)
    row1 = mat.row(0)
    row2 = mat.row(1)

    assert_equal row1, array[0], "First row wasn't equal"
    assert_equal row2, array[1], "Second row wasn't equal"
  end

  def test_get_column_tridiagonal
    d1 = [1]
    d2 = [4, 8]
    d3 = [77]
    mat = SparseMatrix.new(d1, d2, d3)
    col1 = mat.column(0)
    col2 = mat.column(1)
    assert_equal col1, [4, 77], "Column from tridiagonal matrix wasn't returned correctly"
    assert_equal col2, [1, 8], "Column from tridiagonal matrix wasn't returned correctly"
  end

  def test_get_row_tridiagonal
    d1 = [1]
    d2 = [4, 8]
    d3 = [99]
    mat = SparseMatrix.new(d1, d2, d3)
    row1 = mat.row(0)
    row2 = mat.row(1)
    assert_equal row1, [4, 1], "Row from tridiagonal wasn't correctly returned"
    assert_equal row2, [99, 8], "Row from tridiagonal wasn't correctly returned"
  end

  def test_to_matrix
    mat = @test.to_matrix
    zeros = Matrix.zero(4)
    assert mat != nil
    assert_equal @test, SparseMatrix.new(mat)
    assert_equal mat, zeros
  end

  def test_each_non_zero_tri_diagonal
    d1 = [4, 8, 15]
    d2 = [16, 23, 42, 108]
    d3 = [108, 10, 99]
    matrix = SparseMatrix.new(d1, d2, d3)
    a = Array.new
    matrix.each_non_zero { |x| a.push(x)}
    assert_equal d1+d2+d3 ,a
  end

  def test_to_s_empty
    str = @test.to_s
    answer = "SparseMatrix[[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]"
    assert_equal answer, str
  end

  def test_to_s_success
    arr = SparseMatrix.new([
      [1, 2, 8],
      [2, 1, 4],
      [4, 8, 15162342]])
   
    str =  "SparseMatrix[[1, 2, 8], [2, 1, 4], [4, 8, 15162342]]"
    assert_equal arr.to_s, str
  end

  def test_to_s_tridiagonal
    d1 = [1,2,3]
    d2 = [1,2,4,4]
    mat = SparseMatrix.new(d1, d2, d1)
    str = "SparseMatrix[[1, 1, 0, 0], [1, 2, 2, 0], [0, 2, 4, 3], [0, 0, 3, 4]]"

    assert_equal mat.to_s, str 
  end

  def test_hash_success
    hash = @test.hash
    same_hash = SparseMatrix.new(4).hash
    assert_equal hash, same_hash, "Hashes of identical matrices aren't the same"
  end

  def test_hash_tridiagonal
    d1 = [4,8,15,16,23,42]
    d2 = [4,8,15,16,23,42,108]
    hash = SparseMatrix.new(d1, d2, d1).hash
    same_hash = SparseMatrix.new(d1, d2, d1).hash
    assert_equal hash, same_hash, "Hashes of identical tridiagonal matrices aren't the same"
  end

  def test_diagonal_zeros
    diagonal = @test.diagonal
    assert_equal diagonal, [0, 0, 0, 0]
  end

  def test_diagonal_success
    mat = SparseMatrix.new([
      [4, 4, 4, 4, 4, 4],
      [8, 8, 8, 8, 8, 8],
      [15, 15, 15, 15, 15, 15],
      [16, 16, 16, 16, 16, 16],
      [23, 23, 23, 23, 23, 23],
      [42, 42, 42, 42, 42, 42]])
    assert_equal mat.diagonal, [4, 8, 15, 16, 23, 42]
  end

  def test_diagonal_tridiagonal
    d1 = [1]
    d2 = [1, 2]
    d3 = [1]
    mat = SparseMatrix.new(d1, d2, d3)
    assert_equal mat.diagonal, d2
  end

  def test_each_non_zero
    mat = SparseMatrix.new([
      [4, 0],
      [6, 9]])

    non_zeros = [4, 6, 9]
    assert_block "Non zero elements were invalid" do
      mat.each_non_zero{|x| non_zeros.include?(x)}
    end
  end

  def test_each_non_zero_tridiagonal
    d1 = [4, 8, 15, 16, 23, 42]
    d2 = [4, 8, 15, 16, 23, 42, 108]
    mat = SparseMatrix.new(d1, d2, d1)

    non_zeros = d1 + d2
    assert_block "Non zero elements were invalid" do
      mat.each_non_zero{|x| non_zeros.include?(x)}
    end
  end

  def test_tri_to_a
    d1 = [4, 8]
    d2 = [15, 16, 23]
    tri = SparseMatrix.new(d1, d2, d1)
    array = [[15, 4, 0], [4, 16, 8], [0, 8, 23]]
    assert_equal array, tri.to_a
  end

  def test_tri_to_matrix
    d1 = [4, 8]
    d2 = [15, 16, 23]
    tri = SparseMatrix.new(d1, d2, d1)
    bat = Matrix.rows([[15, 4, 0], [4, 16, 8], [0, 8, 23]])
    assert_equal bat, tri.to_matrix
  end

  def test_get_success
    for r in 0..@test.rows-1
      for c in 0..@test.cols-1
        assert_equal 0, @test.get(r,c), "Empty array isn't all 0s"
      end
    end
  end

  def test_get_tridiagonal_success
    d1 = [4, 8]
    d2 = [15, 16, 23]
    tri = SparseMatrix.new(d1, d2, d1)
    assert_equal 4, tri.get(0,1)
    assert_equal 4, tri.get(1,0)
    assert_equal 15, tri.get(0,0)
    assert_equal 16, tri.get(1,1)
    assert_equal 23, tri.get(2,2)
    assert_equal 8, tri.get(1,2)
    assert_equal 8, tri.get(2,1)
  end

  def test_put_success
    mat = SparseMatrix.new(4)
    mat.put(0,0,4815162342)
    assert_equal 4815162342, mat.get(0,0)
  end

  def test_put_tridiagonal_success
    d1 = [4, 8]
    d2 = [15, 16, 23]
    tri = SparseMatrix.new(d1, d2, d1)
    tri.put(0,0, 108)
    assert_equal 108, tri.get(0,0)
  end

  def test_sparsity_zeros
    assert_equal @test.sparsity, 0
  end

  def test_sparsity_success
    mat = SparseMatrix.new([
      [0, 1, 2],
      [0, 1, 0],
      [0, 0, 0]])
    assert_equal mat.sparsity, 1.0/3
  end

  def test_sparsity_tridiagonal
    d1 = [1, 2, 2]
    d2 = [1, 2, 3, 4]
    mat = SparseMatrix.new(d1, d2, d1)
    assert_equal mat.sparsity, 0.625
  end

end
