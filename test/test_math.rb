require 'test/unit'
require './sparsematrix'

class TestSparseMatrixMath < Test::Unit::TestCase   

  def setup 
    @zeros= SparseMatrix.new(4)
    @ones = SparseMatrix.new([
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1]])
    @numbers = SparseMatrix.new([
      [4, 8, 15, 16],
      [23, 42, 4, 8],
      [15, 16, 23, 42],
      [4, 8, 15, 16]])
    # this is so funky!!!
    @funky = SparseMatrix.new([
      [1, 2],
      [3, 4]])
  end

  def test_simple_addition
    result = @zeros + @ones
    assert_equal result, @ones, "Matrices weren't equal"
  end

  def test_addition_two_empty_matrices
    mat = SparseMatrix.new(4)
    result = @zeros + mat
    assert_equal @zeros, mat
  end

  def test_addition_success
    result = @ones + @numbers
    answer = SparseMatrix.new([
      [5, 9, 16, 17],
      [24, 43, 5, 9],
      [16, 17, 24, 43],
      [5, 9, 16, 17]])
    assert_equal result, answer
  end

  def test_simple_subtraction
    result = @ones - @zeros
    assert_equal result, @ones, "Matrices weren't equal"
  end

  def test_subtraction_two_empty_matrix
    result = @zeros - @zeros
    assert_equal result, @zeros
  end

  def test_subtraction_success
    result = @numbers - @ones
    answer = SparseMatrix.new([
      [3, 7, 14, 15],
      [22, 41, 3, 7],
      [14, 15, 22, 41],
      [3, 7, 14, 15]])
  end

  def test_add_tridiagonal_matrix
    mat = SparseMatrix.new([[4, 4], [1, 1]])
    assert mat.tridiagonal?, "Matrix isn't tridiagonal"
    result = @funky + mat
    assert_equal result.to_a, [[5, 6], [4, 5]]
  end

  def test_multiply_with_zeros
    result = @zeros * @ones
    assert_equal result, @zeros
  end

  def test_multiply_success
    result = @numbers * @numbers
    answer = SparseMatrix.new([
      [489, 736, 677, 1014],
      [1150, 2076, 725, 1000],
      [941, 1496, 1448, 2006],
      [489, 736, 677, 1014]])
    assert_equal result, answer 
  end

  def test_multiply_vector_ones
    vector = [4, 8, 15, 16]
    result = @ones * vector
    answer = Vector[43, 43, 43, 43]
    assert_equal result, answer
  end

  def test_multiply_vector
    vector = [4, 8, 15, 16]
    result = @numbers * vector
    answer = Vector[561, 616, 1205, 561]
    assert_equal result, answer
  end

  def test_divide_success
    result = @funky / @funky 
    answer = SparseMatrix.new([
      [1, 0],
      [0, 1]])
    assert_equal result, answer
  end

  def test_divide_zeros
    matrix = SparseMatrix.new([
      [1, 2, 3, 4],
      [8, 15, 16, 23],
      [42, 108, 99, 64],
      [14, 4, 94, 1]])
    result = @zeros / matrix
    assert_equal result, @zeros
  end

  def test_determinant_zeros
    assert_equal @numbers.det, 0
    assert_equal @ones.det, 0
    assert_equal @zeros.det, 0
  end

  def test_determinant_success
    result = @funky.det
    assert_equal result, -2
  end

  def test_inverse_success
    matrix = SparseMatrix.new([
      [1, 2, 3, 4],
      [8 ,15, 16 ,23],
      [42, 108, 99 ,93],
      [14, 4, 10, 37]])
    result = matrix.inv
    assert_equal result, SparseMatrix.new(matrix.to_matrix.inv) 
  end

end
