class SparseMatrix::Storage::SparseMatrixStorage

  include SparseMatrix::Contracts::SparseMatrixStorageContracts

  attr_reader :values, :rows, :cols

  # creates empty matrix of size row x col
  def initialize(row, col)
    initialize_preconditions(row, col)
    @values= Hash.new(0)
    @rows = row
    @cols = col
    class_invariant()
    initialize_postconditions(row, col)
  end

  # initializes the matrix to the elements in the array
  def make_from_array(array)
    init_array_preconditions(array)
    class_invariant()
    for r in 0..array.length-1
      for c in 0..array[0].length-1
        if array[r][c] != 0
          @values[[r,c]] = array[r][c]
        end
      end
    end
    @rows = array.length
    @cols = array[0].length
    class_invariant()
    init_array_postconditions(array)
  end

  # returns the element at coordinates (row, col)
  def get(row,col)
    get_preconditions(row, col)
    class_invariant()
    return @values[[row, col]]
  end

  # updates the value at coordinate (row, col) to val
  def put(row,col, val)
    put_preconditions(row, col, val)
    class_invariant()
    values[[row, col]] = val
  end

  # enumerates all non-zero elements of matrix
  def each_non_zero
    @values.each {|k,v| yield v if block_given?}
  end

  # returns hash of matrix
  def hash
    class_invariant()
    return 97 * @values.hash ^ rows.hash ^ cols.hash * 71
  end

  # returns sparsity of matrix
  def sparsity
    class_invariant()
    total = @rows*@cols
    non_zero = @values.length
    sparsity_postconditions(total, non_zero)
    return 1.0*non_zero/total
  end

end
