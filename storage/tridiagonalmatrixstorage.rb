class SparseMatrix::Storage::TriDiagonalMatrixStorage

  include SparseMatrix::Contracts::TridiagonalMatrixContracts

  attr_reader :topDiagonal , :middleDiagonal , :botDiagonal, :rows , :cols

  UPPER_DIAGONAL = -1
  MIDDLE_DIAGONAL = 0
  LOWER_DIAGONAL = 1
  INVALID_DIAGONAL = 2

  # creates a tridiagonal array with the diagonals d1, d2, d3
  def initialize(d1, d2, d3)
    initialize_diagonal_preconditions(d1,d2,d3)
    @topDiagonal = d1
    @middleDiagonal = d2
    @botDiagonal = d3
    @rows  = middleDiagonal.length 
    @cols = middleDiagonal.length 
    class_invariant()
    initialize_diagonal_postconditions(d2, self)
  end

  # Returns the element at coordinates (i,j)
  def get(i,j)
    get_preconditions(i,j)
    class_invariant()
    if(findDiagonal(i,j) == LOWER_DIAGONAL)
      return botDiagonal[i-1]
    elsif(findDiagonal(i,j) == MIDDLE_DIAGONAL)
      return middleDiagonal[i]
    elsif(findDiagonal(i,j) == UPPER_DIAGONAL)
      return topDiagonal[j-1]
    else
      return 0
    end
  end

  # returns what diagonal the coordinate (i,j) is on
  def findDiagonal(i,j)
    class_invariant()
    if (i-j) == 1
      return LOWER_DIAGONAL
    elsif (i-j) == 0
      return MIDDLE_DIAGONAL
    elsif (i-j) == -1
      return UPPER_DIAGONAL
    else return INVALID_DIAGONAL
    end
  end

  # returns the hash representation of the matrix
  def hash
    class_invariant()
    return 91 * @topDiagonal.hash * rows.hash ^ middleDiagonal.hash * cols.hash ^ botDiagonal.hash
  end


  # updates the value of coordinate (i, j) to k
  def put(i, j, k)
    put_tridiagonal_preconditions(i, j, k)
    class_invariant()
    if(findDiagonal(i,j) == LOWER_DIAGONAL)
      botDiagonal[i-1] = k 
    elsif(findDiagonal(i,j) == MIDDLE_DIAGONAL)
      middleDiagonal[i] = k
    elsif(findDiagonal(i,j) == UPPER_DIAGONAL)
      topDiagonal[j-1] = k
    end
    class_invariant()
    put_tridiagonal_postconditions(i, j, k)
  end

  # enumerates all non_zero elements of the tridiagonal matrix
  def each_non_zero 
    class_invariant()
    allRows = topDiagonal + middleDiagonal + botDiagonal
    allRows.each { |element|
        yield element if block_given?
     }
  end

  # enumerates all non-zero elements of matrix along with their coordinate
  def each_non_zero_with_index
    class_invariant()
    topDiagonal.each_with_index do |x, xi|
      yield x, xi , xi+1
    end
    middleDiagonal.each_with_index do |x,xi|
      yield x,xi,xi
    end
    botDiagonal.each_with_index do |x,xi|
      yield x, xi+1 , xi
    end
  end

  # returns sparsity of matrix
  def sparsity
    class_invariant
    total = middleDiagonal.length*middleDiagonal.length
    non_zero = middleDiagonal.length+2*topDiagonal.length
    sparsity_postconditions(total, non_zero)
    return 1.0*non_zero/total
  end

end
