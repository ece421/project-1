require './sparselib'

class SparseMatrix

  include SparseMatrix::Factory::SparseMatrixFactory
  include SparseMatrix::Factory::TridiagonalMatrixFactory
  include SparseMatrix::Contracts::SparseMatrixContracts

  attr_reader :data
  attr_reader :rows
  attr_reader :cols

  # creates a SparseMatrix with the given arguments
  def initialize(*args)
    initialize_preconditions(args)
    if args.size == 1
      if args[0].is_a?(Integer)
        @data = Factory::SparseMatrixFactory.make_empty(args[0], args[0])
      elsif args[0].is_a?(Matrix)
        if args[0].square? && Factory::TridiagonalMatrixFactory.tridiagonal_array?(args[0].to_a)
          @data = Factory::TridiagonalMatrixFactory.make_from_matrix(args[0])
        else
          @data = Factory::SparseMatrixFactory.make_from_matrix(args[0])
        end
      elsif args[0].respond_to?(:to_a)
        if args[0].size == args[0][0].size && Factory::TridiagonalMatrixFactory.tridiagonal_array?(args[0])
          @data = Factory::TridiagonalMatrixFactory.make_from_array(args[0])
        else
          @data = Factory::SparseMatrixFactory.make_from_array(args[0])
        end
      end
    elsif args.size == 2
      if args[0].is_a?(Integer) && args[1].is_a?(Integer)
        @data = Factory::SparseMatrixFactory.make_empty(args[0], args[1])
      end
    elsif args.size == 3
      @data = Factory::TridiagonalMatrixFactory.make_from_diagonals(args[0], args[1], args[2])
    end
    
    @rows = @data.rows
    @cols = @data.cols
    class_invariant()
    initialize_postconditions
  end

  # returns true if matrix is tridiagonal
  def tridiagonal?
    if !self.square?
      return false
    end

    class_invariant()

    minimum_tridiagonal_matrix_size = 2
    if @rows < minimum_tridiagonal_matrix_size
      return false
    end

    # Any 0s on the endpoints?
    if @data.get(0,0) == 0 || @data.get(0,0) == 0 || @data.get(@rows-1,@cols-1) == 0 ||
        @data.get(@rows-1, @cols-2) == 0
      return false
    end

    # Any 0s on the diagonals
    for i in 1..@rows-2
      if @data.get(i,i) == 0 || @data.get(i,i-1) == 0 || @data.get(i, i+0) == 0
        return false
      end

      for j in 0..@cols-1
        if @data.get(i,j) != 0
          if !(i-1..i+1).include?(j)
            return false
          end
        end
      end
    end

    class_invariant()
    return true
  end

  # returns the element at coordinates (row, col)
  def get(row, col)
    get_preconditions(row, col)
    class_invariant()
    return @data.get(row, col)
  end

  alias [] get

  # sets the value of the element at (row, col) to val
  def put(row, col, val)
    put_preconditions(row, col, val)
    class_invariant()
    @data.put(row,col,val)
    class_invariant()
    put_postconditions(row, col, val)
  end

  # returns if matrix is square
  def square?
    class_invariant()
    return @rows == @cols
  end

  # returns the ith column
  def column(i)
    column_preconditions(i) 
    class_invariant()
    column = []
    for r in 0..@rows-1
      column << self[r, i]
    end
    class_invariant()
    column_postconditions(column)
    return column
  end

  # returns the ith row
  def row(i)
    row_preconditions(i)
    class_invariant()
    row = []
    for c in 0..@cols-1
        row << self[i, c]
    end
    class_invariant()
    row_postconditions(row)
    return row
  end

  # returns the diagonal
  def diagonal()
    diagonal_preconditions()
    class_invariant()
    diagonal = []
    for i in 0..@cols-1
      diagonal << self[i,i]
    end
    class_invariant()
    diagonal_postconditions(diagonal)
    return diagonal
  end

  # returns the array representation of this matrix
  def to_a
    class_invariant()
    array = Array.new(@rows){Array.new(@cols)} 
    for r in 0..@rows-1
      for c in 0..@cols-1
        array[r][c] = self[r,c]
      end
    end
    class_invariant()
    to_a_postconditions(array)
    return array
  end

  # returns the Ruby Matrix representation of this matrix
  def to_matrix
    result = Matrix.rows(to_a)
    class_invariant()
    to_matrix_postconditions(result)
    return result
  end

  # returns true if every element in matrix is equal to every element in our data
  def ==(matrix)
    if matrix == nil
      return false
    end
    return to_a == matrix.to_a
  end

  # returns the sum of mat and our data
  def +(mat)
    add_preconditions(mat)
    class_invariant()
    return SparseMatrix.new(to_matrix+mat.to_matrix)
  end

  # returns the difference of our data minus mat
  def -(mat)
    add_preconditions(mat)
    class_invariant()
    return SparseMatrix.new(to_matrix-mat.to_matrix)
  end

  # returns the product of our data and m, m can be a scalar, vector, or matrix
  def *(m)
    mult_preconditions(m)
    class_invariant()
    if m.is_a?(Integer) # Scalar multiplication
      result = SparseMatrix.new(to_matrix*m)
    elsif !m.respond_to?(:to_matrix) # Vector multiplication
      a = to_matrix*Vector.elements(m)
      result = a 
    else  # Matrix multiplication
      result = SparseMatrix.new(to_matrix*m.to_matrix)
    end
    class_invariant()
    mult_postconditions(m, result)
    return result
  end

  # divides our data by m
  def /(m)
    div_preconditions(m)
    class_invariant()
    result = self*m.inv
    class_invariant()
    div_postconditions(result)
    return result
  end

  # returns inverse of our data
  def inv
    inv_preconditions
    class_invariant()
    result = SparseMatrix.new(self.to_matrix.inv)
    class_invariant()
    inv_postconditions(result)
    return result
  end

  # returns determinant of our data
  def determinant
    det_preconditions
    class_invariant()
    result = self.to_matrix.det
    class_invariant()
    det_postconditions(result)
    return result
  end

  alias det determinant

  # enumerates through every non-zero element of matrix
  def each_non_zero(&block)
    class_invariant()
    @data.each_non_zero(&block)
  end

  # returns string representation of matrix
  def to_s
    class_invariant()
    return "Sparse" + to_matrix.to_s
  end

  # returns hashcode of matrix
  def hash
    class_invariant()
    return @data.hash
  end

  # returns sparse representation of tridiagonal
  def to_sparse
    if self.tridiagonal?
      matrix = self.to_sparse_from_tridiag(@data)
      return matrix
    end
  end

  # converts matrix from tridiagonal to sparse
  def to_sparse_from_tridiag(matrix)
    sparse = Factory::SparseMatrixFactory.make_empty(matrix.rows, matrix.cols)
    matrix.each_non_zero_with_index {|e,i,j| sparse.put(i,j,e)}
    return sparse
  end

  # returns sparsity of matrix
  def sparsity
    return @data.sparsity
  end

end
