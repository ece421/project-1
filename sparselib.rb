class SparseMatrix
end

class SparseMatrix::Factory
end

class SparseMatrix::Contracts
end

class SparseMatrix::Storage
end

require './contracts/sparsematrixcontracts'
require './contracts/tridiagonalmatrixcontracts'
require './contracts/sparsematrixstoragecontracts'
require './contracts/tridiagfactorycontracts'
require './storage/sparsematrixstorage'
require './storage/tridiagonalmatrixstorage'
require './factory/abstractmatrixfactory'
require './factory/sparsematrixfactory'
require './factory/tridiagonalmatrixfactory'

require 'matrix'
