require 'test/unit'

module SparseMatrix::Contracts::SparseMatrixContracts

  include Test::Unit::Assertions

  # Validates that arguments are valid to create a sparse matrix
  def initialize_preconditions(*args)
    assert args != nil, 'Constructor needs at least one argument'
    assert args.size > 0, 'Constructor needs at least one argument'
    assert args.size < 4
  end

  # Validates that a sparse matrix was created
  def initialize_postconditions
    assert @data != nil, 'Matrix should be created'
  end

  # Validates that coordinates are valid for this matrix
  def get_preconditions(r, c)
    assert r >= 0, "Row cannot be negative"
    assert c >= 0, "Column cannot be negative"

    assert r < @rows, "Row cannot be larger than number of rows"
    assert c < @cols, "Column cannot be larger than number of coloumns"
  end

  # Validates that value returned is in the sparse matrix
  def get_postconditions(value)
    if value != 0
      assert @values.include?(result)
    end
  end

  # validates value to be inserted and coordinates to insert at
  def put_preconditions(r, c, v)
    assert r < @rows, "Row cannot be larger than number of rows"
    assert c < @cols, "Column cannot be larger than number of columns"
    assert r >= 0 && c >= 0, "Row or Column cannot be negative"
    assert v != nil, "Value cannot be nil"
  end

  
  # validates that value was updated
  def put_postconditions(r, c, v)
    assert_equal @data.get(r,c), v, "Value must be changed"
  end

  # validate that array can be converted to a sparse matrix
  def initialize_array_preconditions(array)
    assert array.respond_to?(:[]), "Row Array type object must respond to get"
    assert array.respond_to?(:each), "Row Array type object must respond to each"
    assert array.respond_to?(:size), "Row Array type object must respond to size"
    assert array.size > 0, "Array was too small"
    assert array[0].size > 0, "Array was too small"
    assert array[0].respond_to?(:[]), "Column Array type object must respond to get"
    assert array[0].respond_to?(:each), "Column Array type object must respond to each"
    assert array[0].respond_to?(:size), "Column Array type object must respond to size"

    max_size = array[0].size
    for i in 0..array.size-1
      assert_equal array[i].size, max_size, "Every column must be the same size"
    end
  end

  # validate that sparsematrix the same as array
  def initialize_array_postconditions(data, array)
    assert_equal data.rows, array.length, "Array and SparseMatrix have different row sizes"
    assert_equal data.cols, array[0].length, "Array and SparseMatrix have different col sizes"
  end

  # validate that matrix can be made into a sparsematrix
  def initialize_matrix_preconditions(matrix)
    assert matrix.respond_to?(:to_a), "Matrix cannot be made into an array"
  end

  # validate that matrix was properly converted into sparsematrix
  def initialize_matrix_postconditions(matrix, data)
    assert_equal matrix.row_size, data.rows, "Array and matrix have different sized rows, oh no"
    assert_equal matrix.column_size, data.cols, 'Array and matrix have different sized rows, oh my'
  end

  # validate row exists at index i
  def row_preconditions(i)
    assert i >= 0, "Row must be greater than 0"
    assert @rows > i, "Row must be less than number of rows"
  end

  # validate that row being returned is valid row
  def row_postconditions(row)
    assert_equal row.length, @cols, "Row must be same size as all other rows"
  end

  # validate that column exists at index i
  def column_preconditions(i)
    assert i >= 0, "Column cannot be negative"
    assert @cols > i, "Column must be less than number of columns"
  end

  # validate that column returned is valid
  def column_postconditions(col)
    assert_equal col.length, @rows
  end

  # validate we can multiply mult by our sparsematrix
  def mult_preconditions(mult)
    if mult.is_a? SparseMatrix
      assert_equal @rows, mult.cols, "Number of rows doesn't equal number of columns for multiplication"
    elsif mult.is_a? Array
      assert_equal mult.length, @rows, "Number of rows doesn't equal length of array for multiplication"
    end 
  end

  # validate product of m and sparse matrix 
  def mult_postconditions(m, result)
      assert result != nil
      if !m.respond_to?(:to_matrix)
        assert_equal result.size, @rows
      else
        assert_equal @rows, result.rows, "Multiplying matrix changed number of rows"
        assert_equal @cols, result.cols, "Multiplying matrix changed number of cols"
      end
  end

  # Validate that we can divide our sparsematrix by div
  def div_preconditions(div)
    assert div.respond_to?(:inv), "Matrix didn't know about no inverse!"
    assert_equal div.rows, @rows, "Matrices have different number of rows"
    assert_equal div.cols, @cols, "Matrices have different number of cols"
  end

  # validate quotient
  def div_postconditions(div)
    assert_equal div.rows, @rows, "Matrices had different number of rows"
    assert_equal div.cols, @cols, "Matrices had different number of cols"
  end

  # validate that we can add adder to our sparsematrix
  def add_preconditions(adder)
    assert_equal adder.rows, @rows, "Matrices have different number of rows"
    assert_equal adder.cols, @cols, "Matrices have different number of cols"
  end

  # validate that we can take that inverse of our sparsematrix
  def inv_preconditions()
    # really doubt we call it like this
    assert square?, "Matrix wasn't square"
    assert determinant != 0, "Determinant of matrix was 0"
  end

  # validate inverse of matrix
  def inv_postconditions(result)
    hopefully_identity = self * result
    assert_block "Matrix times its inverse wasn't idenity matrix" do
      hopefully_identity.each_non_zero{|x| x == 1}
    end
  end

  # validate that we can compute determinant of matrix
  def det_preconditions()
    assert square?, "Matrix wasn't square"
  end

  # validate determinant of matrix
  def det_postconditions(result)
    assert result != nil, "Determinant was nil!"
    assert result.is_a?(Integer), "Determinant wasn't an Integer"
  end

  # validate array produced by sparsematrix
  def to_a_postconditions(array)
    assert_equal array.length, @rows, "Array and matrix have different number of rows"
    assert_equal array[0].length, @cols, "Array and matrix have different number of columns"
  end

  # validate yielded value
  def each_postconditions(result)
    assert result != nil, "Each returned a nil"
  end

  # validate Ruby Matrix produced by sparsematrix
  def to_matrix_postconditions(result)
    assert_equal result.row_size, @rows, "Rows of Matrix and SparseMatrix aren't equal"
    assert_equal result.column_size, @cols, "Columns of Matrix and SparseMatrix aren't equal"
  end

  # validate that diagonal of matrix is possible to compute
  def diagonal_preconditions()
    assert square?, "You can't have a diagonal if your matrix isn't square!"
  end

  # validate diagonal of spasematrix
  def diagonal_postconditions(diagonal)
    assert_equal diagonal.size, @rows, "Diagonal wasn't same size as rows"
    assert_equal diagonal.size, @cols, "Diagonal wasn't same size as cols"
  end

  # Invariants for sparsematrix class that must always be true
  def class_invariant()
    @data.class_invariant()
  end
end
