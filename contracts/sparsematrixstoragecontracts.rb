require 'test/unit'

module SparseMatrix::Contracts::SparseMatrixStorageContracts

  include Test::Unit::Assertions

  # validate sparsematrix can be created with row rows and col columns
  def initialize_preconditions(row, col)
    assert row > 0, "Row size was too small"
    assert col > 0, "Col size was too small"
  end

  # validate sparsematrix created was of the correct size
  def initialize_postconditions(row, col)
    assert_equal @rows, row, "We somehow made a matrix with different sized rows"
    assert_equal @cols, col, "We somehow made a matrix with differnt sized cols" 
  end
  
  # Variants that must always remain true
  def class_invariant()
    assert @rows != nil
    assert @cols != nil
    assert @values != nil
    assert @rows >= 0, "Number of rows is too small"
    assert @cols >= 0, "Number of cols is too small"
  end

  # validate that sparsematrix can be created from array
  def init_array_preconditions(array)
    assert array != nil, "Array was nil"
    assert array.size > 0, "Array is too small"
    assert array[0].size > 0, "Array is too small"
  end

  # validate sparsematrix created from array
  def init_array_postconditions(array)
    assert_equal @rows, array.length, "Rows didn't match up"
    assert_equal @cols, array[0].length, "Cols didn't match up"
  end

  # validate element exists at coordinates (i,j)
  def get_preconditions(i,j)
    assert i >= 0 && j >= 0, "Matrix cannot have negative coordinates"
    assert i < @rows && j < @cols, "Coordinate was larger than matrix"
  end

  # validate that we can update element at (i,j) to value k
  def put_preconditions(i,j,k)
    get_preconditions(i, j)
    assert k != nil
  end

  # validate that we can return the sparsity of matrix
  def sparsity_postconditions(total, non_zero)
    assert total != nil && non_zero != nil
    assert total > 0, "Total number of elements is zero"
  end

end
