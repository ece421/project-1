require 'test/unit'

module SparseMatrix::Contracts::TridiagonalMatrixContracts

  include Test::Unit::Assertions 

  # validate tridiagonal matrix can be created with provided diagonals
  def initialize_diagonal_preconditions(d1, d2, d3)
    assert d1.length != 0, "Diagonal was too small"
    assert d2.length != 0, "Diagonal was too small"
    assert d3.length != 0, "Diagonal was too small"
    assert d1.length == d2.length - 1, "Middle diagonal was not one element larger"
    assert d3.length == d2.length - 1, "Middle diagonal was not one element larger"
    assert_block 'Zero value in diagonal' do
      d1.each {|value| return false unless value != 0}
    end
    assert_block 'Zero value in diagonal' do
      d2.each {|value| return false unless value != 0}
    end
    assert_block 'Zero value in diagonal' do
      d3.each {|value| return false unless value != 0}
    end
  end

  # validate tridiagonal matrix created 
  def initialize_diagonal_postconditions(middle_diagonal, data)
    assert data != nil
    assert_equal middle_diagonal.length, data.rows, "Tridiagonal matrix has different size than its diagonal"
    assert_equal middle_diagonal.length, data.cols, "Tridiagonal matrix has different size than its diagonal"
  end

  # validate element at coordinates (i,j)
  def get_preconditions(i,j)
    assert j >= 0 && i >= 0, "Coordinates were too small"

    assert i < @rows && j < @cols, "Coordinates were too big"
  end

  # validate element k can be inserted at coordinate (i,j)
  def put_tridiagonal_preconditions(i,j,k)
    assert findDiagonal(i,j) != SparseMatrix::Storage::TriDiagonalMatrixStorage::INVALID_DIAGONAL, 
      "Coordinates were not on a diagonal"  
    assert k != 0, "Can't change value to 0"
  end

  # validate element at (i,j) was updated to k
  def put_tridiagonal_postconditions(i,j,k)
    assert_equal get(i,j), k, "Value was not changed"
  end

  # Class variants that must always remain true
  def class_invariant()
    assert_equal @topDiagonal.length, @botDiagonal.length, "Bottom and top diagonals differ in length"
    assert @topDiagonal.length + 1 ==  @middleDiagonal.length, "Top diagonal is not 1 less than middle"
    assert @botDiagonal.length + 1 == @middleDiagonal.length, "Bottom diagonal is not 1 less than middle"
    assert_block 'Zero value in diagonal' do
      @topDiagonal.each {|value| return false unless value != 0}
    end
    assert_block 'Zero value in diagonal' do
      @middleDiagonal.each {|value| return false unless value != 0}
    end
    assert_block 'Zero value in diagonal' do
      @botDiagonal.each {|value| return false unless value != 0}
    end
    
  end

  # Validate we can return sparsity
  def sparsity_postconditions(total, non_zero)
    assert total != nil && non_zero != nil
    assert total > 0, "Total number of elements is 0"
  end

end
