require 'test/unit'

module SparseMatrix::Contracts::TriDiagFactoryContracts

  include Test::Unit::Assertions
  
  # validate tridiagonal matrix can be created with provided diagonals
  def initialize_diagonal_preconditions(d1,d2,d3)
    assert d1.length > 0, "d1 must not be zero"
    assert d2.length > 0, "d2 must not be zero"
    assert d3.length > 0 ,"d3 must not be zero"
    assert d1.length == d2.length - 1
    assert d3.length == d2.length - 1
  end

  # validate tridiagonal matrix created matches diagonals provided
  def initialize_diagonal_postconditions(d1,d2,d3,data)
    assert data.topDiagonal.length ==  d1.length, "Diagonals d1 not same length"
    assert data.middleDiagonal.length  == d2.length, "Diagonals d2 not same length"
    assert data.botDiagonal.length  == d3.length , "Diagonals d3 not same length"
    assert data.rows  == d2.length , "Array not right size"
    assert data.cols  == d2.length , "Array not right size"
  end

  # validate tridiagonal matrix can be created with provided array
  def init_array_preconditions(anArray)
    assert anArray != nil
    assert SparseMatrix::Factory::TridiagonalMatrixFactory.tridiagonal_array?(anArray), "Array is not tridiagonal"
  end

  # validate tridiagonal matrix created from array
  def init_array_postconditions(array, data)
    assert data!=nil
    assert_equal array.length, data.rows, "Matrix created is different size than array"
    assert_equal array.length, data.cols, "Matrix created is different size than array"
  end

  # validate tridiagonal matrix can be created with provided matrix
  def init_mat_preconditions(matrix)
    assert matrix != nil
    assert SparseMatrix::Factory::TridiagonalMatrixFactory.tridiagonal_array?(matrix.to_a), "Matrix is not tridiagonal"
  end

  # validate tridiagonal matrix created from matrix
  def init_mat_postconditions(mat, data)
    assert data!=nil
    assert_equal mat.row_size, data.rows, "Matrix has different number of rows"
    assert_equal mat.column_size, data.cols, "Matrix has different number of columns"
  end

  # validate array can be checked if tridiagonal
  def tridiagonal_array_preconditions(array)
    assert array != nil
    assert array.length > 0, "Array has too few rows to be tridiagonal"
    assert array[0].length > 0, "Array has too few columns to be tridiagonal"
  end 

end
