module SparseMatrix::Factory::TridiagonalMatrixFactory

  class << self
    include SparseMatrix::Factory::AbstractMatrixFactory
    include SparseMatrix::Contracts::TriDiagFactoryContracts
  end

  # create a tridiagonal matrix from the provided diagonals
  def self.make_from_diagonals(d1, d2, d3)
    initialize_diagonal_preconditions(d1, d2, d3)
    data = SparseMatrix::Storage::TriDiagonalMatrixStorage.new(d1,d2,d3)
    initialize_diagonal_postconditions(d1,d2,d3,data)
    return data
  end


  # Create a tridiagonal matrix from a matrix
  def self.make_from_array(anArray)
    init_array_preconditions(anArray)
    topDiag = Array.new
    midDiag = Array.new
    botDiag = Array.new
    anArray.each_with_index do |x, xi|
      x.each_with_index do |y,yi|
        if (xi-yi) == 1 
          botDiag.push(y)
        elsif (xi-yi) == 0
          midDiag.push(y)
        elsif (xi-yi) == -1
          topDiag.push(y)
        end
      end
    end
    data = SparseMatrix::Storage::TriDiagonalMatrixStorage.new(topDiag,midDiag,botDiag)
    init_array_postconditions(anArray, data)
    return data
  end

  # create a tridiagonal matrix from a Ruby Matrix
  def self.make_from_matrix(mat)
    init_mat_preconditions(mat)
    topDiag = Array.new
    midDiag = Array.new
    botDiag = Array.new
    mat.each_with_index do |e, xi, yi|
        if (xi-yi) == 1 
          botDiag.push(e)
        elsif (xi-yi) == 0
          midDiag.push(e)
        elsif (xi-yi) == -1
          topDiag.push(e)
        end
    end
    data = SparseMatrix::Storage::TriDiagonalMatrixStorage.new(topDiag,midDiag,botDiag)
    init_mat_postconditions(mat, data)
    return data
  end

  # returns if an array is a tridiagonal matrix
  def self.tridiagonal_array?(array)
    tridiagonal_array_preconditions(array)
    if array.length != array[0].length
      return true
    end
    array.each_with_index do |x, xi|
      x.each_with_index do |y,yi|
        if (y==0)
          if (xi-yi) == 1 || (xi-yi) == 0 || (xi-yi) == -1
            return false
          end
        else
          if (xi-yi) != 1 && (xi-yi) != 0 && (xi-yi) != -1
            return false
          end        
        end
      end
    end
    return true
  end

end
