module SparseMatrix::Factory::AbstractMatrixFactory


  # creates an matrix with the elemnents of multidimensonal array a
  def make_from_array(a)
    raise RuntimeException, 'init_array not implemented'
  end

  # creates a sparsematrix with the elements of Ruby Matrix m
  def make_from_matrix(m)
    raise RuntimeException, 'init_matrix not implemented'
  end

end
