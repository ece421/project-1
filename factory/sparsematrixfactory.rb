module SparseMatrix::Factory::SparseMatrixFactory

  class << self
    include SparseMatrix::Factory::AbstractMatrixFactory
    include SparseMatrix::Contracts::SparseMatrixContracts
  end

  # create a rox by col zero matrix
  def self.make_empty(rows, cols)
    return SparseMatrix::Storage::SparseMatrixStorage.new(rows, cols)
  end

  # create a matrix with all of the elements in the multidimensonal array
  def self.make_from_array(array)
    initialize_array_preconditions(array)
    
    data = SparseMatrix::Storage::SparseMatrixStorage.new(array.length, array[0].length)
    data.make_from_array(array)
    initialize_array_postconditions(data, array)

    return data
  end

  # creat a SparseMatrix from the Ruby Matrix class
  def self.make_from_matrix(matrix)
    initialize_matrix_preconditions(matrix)
    data = make_from_array(matrix.to_a)
    initialize_matrix_postconditions(matrix, data)
    return data
  end
end
